Introduction
------------

Create a per-user dashboard that leverages the new Layout Builder module in Drupal 8.7+ Core

Dependencies
------------

This module requires the Layout Builder module first found in Drupal core 8.7.0.

Features
--------

- Per-user dashboard
- Uses Layout Builder
- Creates a unique entity call Dashboard (hence the name)
- Creates a new entity for each user once they are validated
- Have a default dashboard that new user will get
- Limit the blocks that can be added to the dashboard
- Option to redirect the user to dashboard when they log in

Current Maintainers
-------------------
- [istryker](https://www.drupal.org/u/istryker): Tyler Struyk

Supporting organizations
------------------------
- [University of Waterloo](https://uwaterloo.ca/web-resources/) - Module Development
